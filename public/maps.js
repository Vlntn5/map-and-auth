const map = L.map("map");
map.setView([45.790416, 15.9477906], 15);
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(map);

var dataholder = document.getElementById("hidden-data");
if (dataholder) {
    for (var element of dataholder.children) {
        if(element.dataset.latitude && element.dataset.longitude)
            L.marker([element.dataset.latitude, element.dataset.longitude]).bindPopup(element.dataset.username + " , " + element.dataset.logtime).addTo(map);
    }
}

function geoFindMe() {

    const status = document.querySelector('#status');

    function success(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        status.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;

        sendDataToServer(latitude, longitude);
        getMap(latitude, longitude);
    }

    function error() {
        status.textContent = 'Unable to retrieve your location';
    }

    if (!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }

}

function getMap(latitude, longitude) {
    map.setView([latitude, longitude], 15);
    L.marker([latitude, longitude]).bindPopup("You").addTo(map);
}

function sendDataToServer(latitude, longitude) {
    var userData = document.getElementById("hidden-user-data");

    if (userData && userData.dataset.username != '') {
        var url = userData.dataset.callback;
        var data = {
            username: userData.dataset.username,
            latitude: latitude,
            longitude: longitude
        }

        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify({
            data: data
        }));
    }
}