const port = process.env.PORT || 4080;
const https = require('https');
const fs = require('fs');

Date.prototype.today = function () {
    return ((this.getDate() < 10) ? "0" : "") + this.getDate() + "/" + (((this.getMonth() + 1) < 10) ? "0" : "") + (this.getMonth() + 1) + "/" + this.getFullYear();
}

Date.prototype.timeNow = function () {
    return ((this.getHours() < 10) ? "0" : "") + this.getHours() + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ":" + ((this.getSeconds() < 10) ? "0" : "") + this.getSeconds();
}

const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();

app.use(express.static('public'));
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

app.set('view engine', 'ejs');

const {
    auth
} = require('express-openid-connect');
var loggedUsersData = {};

const config = {
    routes: {
        login: false,
    },
    authRequired: false,
    auth0Logout: true,
    idpLogout: true,
    secret: process.env.SECRET,
    baseURL: process.env.BASE_URL || `https://localhost:${port}`,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: 'https://dev-w5x2d19c.us.auth0.com',
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
        response_type: 'code'
    }
};

app.use(auth(config));

app.get('/', function (req, res) {
    req.user = {
        isAuthenticated: req.oidc.isAuthenticated()
    }
    if (req.user.isAuthenticated) {
        req.user.name = req.oidc.user.name;
    }

    res.render('index', {
        user: req.user,
        loggedUsersData: loggedUsersData,
        urlcallback: process.env.LOCATION_STORAGE || "https://localhost:4080/send-location"
    });
})

app.post('/send-location', function (req, res) {
    if (loggedUsersData[req.oidc.accessToken.access_token]
        && !loggedUsersData[req.oidc.accessToken.access_token].latitude
        && !loggedUsersData[req.oidc.accessToken.access_token].longitude) {

        console.log("Received location :: ");
        console.log(req.body);

        var {
            latitude,
            longitude
        } = req.body.data;

        if (Object.keys(loggedUsersData).length == 6) {
            var first = true;
            var minvalue, minkey;
            for (const [key, value] of Object.entries(loggedUsersData)) {
                if (first) {
                    minvalue = value;
                    minkey = key;
                    first = false;
                } else {
                    if (value < minvalue) {
                        minvalue = value;
                        minkey = key;
                    }
                }
            }

            console.log("Removing ::: ");
            console.log(loggedUsersData[minkey]);
            delete loggedUsersData[minkey];
        }

        loggedUsersData[req.oidc.accessToken.access_token].latitude = latitude;
        loggedUsersData[req.oidc.accessToken.access_token].longitude = longitude;

        console.log("Logged users :: ");
        console.log(loggedUsersData)

        res.status(201).send("Success");
    } else {
        console.log(":: Ignoring duplicate data ::")
        res.status(200).send("Already there");
    }
});

app.get('/login', (req, res) => res.oidc.login({
    returnTo: '/note-login'
}));

app.get('/note-login', function (req, res) {
    var newDate = new Date();

    loggedUsersData[req.oidc.accessToken.access_token] = {
        logtime: newDate.today() + " @ " + newDate.timeNow(),
        name: req.oidc.user.name,
        timenoformat: Date.now()
    }

    res.redirect('/');
})

if (process.env.PORT) {
    app.listen(port, function () {
        console.log(`Server running at https://localhost:${port}/`);
    });
} else {
    https.createServer({
            key: fs.readFileSync('./certificates/server.key'),
            cert: fs.readFileSync('./certificates/server.cert')
        }, app)
        .listen(port, function () {
            console.log(`Server running at https://localhost:${port}/`);
        });
}